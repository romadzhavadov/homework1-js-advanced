class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name; 
    }
    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age; 
    }
    set age(value) {
        this._age = value;
    }

    get salary() {
        return this._salary; 
    }
    set salary(value) {
        this._salary = value;
    }
}

const emp = new Employee("john", 25, 1000);
console.log(emp)
console.log(emp.name)

class Programmer extends Employee {
    constructor(name, age, salary, lang){
        super(name, age, salary);
        this._lang = lang;
    }

    get lang() {
        return this._lang; 
    }
    set lang(value) {
        this._lang = value;
    }

    get salary() {
        return this._salary * 3;
    }
}

const pro1 = new Programmer('Allan', 27, 1500, 'english');
const pro2 = new Programmer('Toni', 30, 1200, 'english');
const pro3 = new Programmer('Bob', 31, 2000, 'english');

console.log(pro1.salary);
console.log(pro2);
console.log(pro3);

